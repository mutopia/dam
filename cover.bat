:: COVER.BAT instruments your JS code using jscoverage and performs the necessary modifications.
::
:: The only dependency is having jscoverage.exe on your system path. Get it from:
:: http://siliconforks.com/jscoverage/download.html
:: 
:: 1. Set the OUT_DIR variable to your desired target directory for the instrumented code.
::    // TODO(orlade): Allow command line argument input.
:: 2. Run the script.
:: 3. For each file OUT_DIR/tests/<name>.js, manually replace the "dam/<name>" argument in define()
::    to "cover-<name>". Only replace "dam/<the name of the current file>", not "dam/*". The only
::    exception is module.js, in which all "dam" instances should be changed to "cover-dam".
::    // TODO(orlade): Automate this with Powershell.
:: 4. Open the browser to:
::    http://localhost/path/to/OUT_DIR/jscoverage.html
::      ?url=http://localhost/path/to/util/doh/runner.html?testModule=cover-dam/tests/module
::    // TODO(orlade): Replace runTests.html as well.
::
:: This will run all unit tests in a JSCoverage IFrame and show coverage in the Summary tab.

SET OUT_DIR="../cover-dam"
jscoverage --encoding=UTF-8 --no-instrument=tests . %OUT_DIR%
:: TODO(orlade): powershell "find files, replace names, save files, open browser"