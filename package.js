/**
 * This file determines build parameters for DAM.
 */

var testRegex = /^dam\/tests\/?/;

var profile = {
  resourceTags: {
    amd: function (filename, mid) {
      return /\.js$/.test(filename);
    },

    copyOnly: function (filename, mid) {
      return mid in {
        "dam/package.json": 1,
        "dam/tests": 1
      };
    },

    // Use mini: true in profile to exclude test files in preference to the test tag and copyTests.
    miniExclude: function (filename, mid) {
      return mid === 'dam/package' || testRegex.test(mid);
    },

    test: function (filename, mid) {
      return false;
    }
  }
};
