define([
  'doh/runner',
  'dojo/_base/lang',
  'require',
  // Code under test
  'dam/TestCase'
], function (doh, lang, require, TestCase) {
  var TEST_NAME = 'hello/world';

  var TEST_RUNNER = {
    register: function (name, fixtures, setUp, tearDown) {
      // summary:
      //    A mock register method that simply saves its arguments.
      this.name = name;
      this.fixtures = fixtures;
      // The register method's setUp and tearDown arguments apply only to
      // the whole test case.
      this.setUpClass = setUp;
      this.tearDownClass = tearDown;
    }
  };

  doh.register('dam/tests/TestCase', {
    testCreate_empty: function () {
      var testCase = new TestCase();
      doh.assertEqual('UNNAMED_TEST', testCase.name);
    },

    testCreate_name: function () {
      var testCase = new TestCase(TEST_NAME);
      doh.assertEqual(TEST_NAME, testCase.name);
    },

    testCreate_params: function () {
      var testCase = new TestCase({name: TEST_NAME});
      doh.assertEqual(TEST_NAME, testCase.name);
    },

    testRegister_empty: function () {
      var testRunner = lang.clone(TEST_RUNNER);
      var test = new TestCase(TEST_NAME);
      test.register(testRunner);
      doh.assertEqual(TEST_NAME, testRunner.name);
      doh.assertEqual(0, testRunner.fixtures.length);
      doh.assertEqual(test.setUpClass, testRunner.setUpClass);
      doh.assertEqual(test.tearDownClass, testRunner.tearDownClass);
    },

    testRegister: function () {
      var testRunner = lang.clone(TEST_RUNNER);
      var testMethod = function () {
        return true;
      };
      var test = new TestCase({
        testMethod: testMethod
      });

      test.register(testRunner);
      doh.assertEqual(1, testRunner.fixtures.length);
      var registeredTest = testRunner.fixtures[0];
      doh.assertEqual('testMethod', registeredTest.name);
      doh.assertEqual(test.setUp, registeredTest.setUp);
      doh.assertEqual(test.tearDown, registeredTest.tearDown);
      doh.assertEqual(testMethod, registeredTest.runTest);
    },

    testRegister_noRunner: function () {
      // summary:
      //    Checks that the register method throws a meaningful error if no test
      //    runner is provided.
      try {
        new TestCase().register();
      } catch (e) {
        doh.assertEqual('No test runner provided for registration.', e.message);
      }
    },

    testRegister_nonFunction: function () {
      // summary:
      //    Tests that non-function properties are not registered as tests.
      var testRunner = lang.clone(TEST_RUNNER);
      new TestCase({
        testLolJustKidding: 'This is not a test.'
      }).register(testRunner);
      doh.assertEqual(0, testRunner.fixtures.length);
    }
  });
});
