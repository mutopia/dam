define([
  'doh/runner',
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dam/TestCase',
  // Code under test
  'dam/Mocking'
], function (doh, declare, lang, TestCase, Mocking) {
  var ERROR_METHOD = function () {
    // summary:
    //    Fails when called.
    throw new Error('testMethod was not mocked.');
  };

  var TRUE_METHOD = function () {
    // summary:
    //    Doesn't fail when called, and returns true to allow equality checking.
    return true;
  };

  var TEST_OBJECT = {
    testMethod: ERROR_METHOD
  };

  // testObject:
  //    Cloned from TEST_OBJECT at the start of each test case allowing it to be
  //    modified without affecting other test cases.
  var testObject;

  new TestCase({
    name: 'dam/tests/Mocking',
    setUp: function () {
      // summary:
      //    Resets the testObject to its original, unmodified state.
      testObject = lang.clone(TEST_OBJECT);
    },

    testMock: function () {
      // Check that the method fails before mocking.
      try {
        testObject.testMethod();
      } catch (e) {
        doh.assertEqual('testMethod was not mocked.', e.message);
      }
      // Check that the method passes after mocking.
      Mocking.mockMethod(testObject, 'testMethod', TRUE_METHOD);
      doh.assertEqual(true, testObject.testMethod());
    },

    testUnmockMethod: function () {
      // summary:
      //    Mocks the test method to fail if called, then unmocks it back to a
      //    passing method. If the unmocking fails, the invocation will fail.
      testObject.testMethod = TRUE_METHOD;
      var handle = Mocking.mockMethod(testObject, 'testMethod', ERROR_METHOD);
      Mocking.unmock(handle);
      doh.assertEqual(true, testObject.testMethod());
    },

    testHandleReset: function () {
      // summary:
      //    Tests that a mocked method can be unmocked by called reset on the
      //    handle.
      testObject.testMethod = TRUE_METHOD;
      var handle = Mocking.mockMethod(testObject, 'testMethod', ERROR_METHOD);
      handle.reset();
      doh.assertEqual(true, testObject.testMethod());
    }
  }).register(doh);
});
