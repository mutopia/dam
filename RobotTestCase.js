define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/aspect',
  'dojo/query',
  'doh/runner',
  'doh/robot',
  './TestCase',
  'jquery'
], function (declare, lang, aspect, query, doh, robot, TestCase, $) {

  return declare([TestCase], {
    // summary:
    //    Extends TestCase to provide convenience methods for controlling the DOH robot.

    // name: String
    //    The default name of this test case to display while testing.
    name: 'UNNAMED_ROBOT_TEST',
    // TODO(orlade): Generate more useful default name from module name.

    // defaultDelay: int
    //    The default number of milliseconds to delay before beginning robot actions with
    //    unspecified delays.
    defaultDelay: 100,

    // defaultDuration: int
    //    The default number of milliseconds to take to complete robot actions with unspecified
    //    durations.
    defaultDuration: 100,

    // timeout: int
    //    The time in milliseconds that the test is allowed to run before timing out and failing.
    //    Assuming robot tests are interactive at a human timescale, this defaults to one minute.
    timeout: 60000,

    // currentTestName: String
    //    The name of the test currently being executed.
    currentTestName: null,

    // currentTestDeferred: Object
    //    The current doh.Deferred that needs to be notified of the success or failure of the
    //    current asynchronous test.
    //
    //    The current test's value is not explicitly nulled after a test completes, because the
    //    processes of resolving the previous test invokes the next test and repopulates this value
    //    synchronously. Therefore to explicitly check whether a test is currently running, use:
    //    this.currentTestDeferred && this.currentTestDeferred.isFulfilled();
    currentTestDeferred: null,

    // robotPromise: Object
    //    A wrapper around a then method, which can be returned from custom robot actions to allow
    //    the test functions to chain a call to 'then' and defer the next block of actions until the
    //    current set is complete. See creation in the constructor for more details.
    robotPromise: null,

    // TODO(aramk): which is better, having functions here which use robot, or subclassing robot?

    constructor: function () {
      // summary:
      //    Creates a RobotPromise for all custom robot actions of this test case to return.
      this.robotPromise = {
        then: lang.hitch(this, function(/*Function*/ callback, /*Integer?*/ delay) {
          // summary:
          //    Provides a convenient way of specifying a set of actions (in the callback function)
          //    which should be executed after the current robot actions are complete.
          // description:
          //    When tests are run, the test runner executes the body of the function synchronously,
          //    and any robot actions are placed in a queue to be executed over the given timeframe.
          //    However if the robot action needs to complete before the next action is possible
          //    (for example, if the robot creates an element that requires interaction), the block
          //    containing that action must be deferred until the action is complete, or else the
          //    eager test runner will throw errors when elements aren't found.
          //
          //    To work around this problem, all custom robot methods of RobotTestCase return this
          //    robotPromise. This contains this single 'then' function which allows the test case
          //    to defer the queueing of the next block of robot actions until the current set is
          //    complete. For example:
          //
          //    this.clickElement(el).then(function() {
          //      // Next set of actions.
          //      // ...
          //      // Chain another .then, or call this.pass();
          //    }
          //
          //    Note that if time is required after the current robot action is completed (e.g. for
          //    loading), the callback may be invoked too early. Thus the optional delay argument is
          //    provided, which will further defer the callback.
          //
          //    Also note that if the callback contains any further actions to queue (i.e. any robot
          //    action requests), then you must wrap *all* following robot actions in the 'then'
          //    function. For example, if you have three actions, [A -> B, C] where B must wait for
          //    A and C should follow both, you would write:
          //
          //    A().then(function() {
          //      B();
          //      C();
          //    });
          //
          //    If C is left outside and after the callback, the C will be queued before the
          //    callback is executed, and hence the final order will be A, callback, C, B.
          // callback:
          //    The callback function to execute once the current set of actions is complete. This
          //    callback is automatically hitched to the scope of the current test case.
          //
          //    The callback is invoked with the current test's doh.Deferred as an argument in case
          //    more complex deferral logic is necessary (in most cases it will not be).
          // delay:
          //    The additional time in milliseconds to wait (e.g. for loading triggered by the
          //    previous action) on top of the time required for the current robot actions.
          //    Defaults to this.defaultDelay.
          delay = this.determineTiming(delay, this.defaultDelay);
          // Wrap the test errback in a function to allow custom arguments to the invocation.
          var errback = lang.hitch(this, function() {
            // Use the Deferred's errback to avoid implicitly passing if an error isn't thrown.
            this.currentTestDeferred.getTestErrback(callback, this)(this.currentTestDeferred);
          });
          robot.sequence(errback, delay);
        })
      };
    },

    mouseToElement: function (/*Element*/ el, /*Integer?*/ delay, /*Integer?*/ duration) {
      // summary:
      //    Moves the mouse to the middle of the given element. Note that if the DOM is modified
      //    while this action is queued such that the target element changes position, the movement
      //    may miss the target; it will move to where the target was when the action was queued.
      // element:
      //    The HTML element to move to.
      // delay:
      //    Number of milliseconds to delay before beginning the transition. Defaults to
      //    defaultDelay.
      // duration:
      //    Number of milliseconds to take to complete the transition. Defaults to defaultDuration.
      delay = this.determineTiming(delay, this.defaultDelay);
      duration = this.determineTiming(duration, this.defaultDuration);
      var center = this._getElementCenter(this.determineElement(el));
      robot.mouseMove(center.x, center.y, delay, duration);
      return this.robotPromise;
    },

    mouseToQuery: function (/*String*/ q, /*Integer?*/ index, /*Integer?*/ delay, /*Integer?*/ duration) {
      // summary:
      //    Moves the mouse to the middle of the element specified by the index'th result matching
      //    the given query string.
      // query:
      //    A standard dojo/query string argument specifying a CSS selector to match.
      // index:
      //    The index of the set of query results to move the mouse to. Defaults to zero.
      // delay:
      //    Number of milliseconds to delay before beginning the transition.
      // duration:
      //    Number of milliseconds to take to complete the transition.
      return this.mouseToElement(this._evaluateQuery(q, index), delay, duration);
    },

    clickElement: function (el, delay, duration, keys) {
      // summary:
      //    Moves the mouse to the given element and clicks on it.
      // keys:
      //    Keys to depress while clicking.
      this.mouseToElement(el, delay, duration);
      return this._click(delay, keys);
    },

    clickDijitButton: function (/*String*/ text, delay, duration) {
      // summary:
      //    Moves the mouse and clicks the dijit/form/Button with the given text.
      return this.clickElement($('.dijitButtonText:contains("' + text + '")')[0], delay, duration);
    },

    clickSilkIcon: function(/*String*/ iconClass, delay, duration) {
      // summary:
      //    Moves the mouse and clicks the element with an icon of the given class. This is specific
      //    to MUtopia, which uses the silkIconFoo class for 'foo' icons.
      return this.clickElement($('.silkIcon' + iconClass)[0], delay, duration);
    },

    clickQuery: function (q, index, delay, duration) {
      // summary:
      //    Moves the mouse and clicks the index'th element matched by the given query.
      return this.clickElement(this._evaluateQuery(q, index), delay, duration);
    },

    _click: function (/*Integer?*/ delay, /*String[]*/ keys) {
      // summary:
      //    Clicks the mouse in whatever location it happens to be. This is private to encourage the
      //    use of higher-level functions like clickElement.
      // delay:
      //    Time in milliseconds to wait before performing the click.
      // keys:
      //    Array of characters or codes of keys to depress while clicking.
      var clickDelay = this.determineTiming(delay, this.defaultDelay);
      if (keys && keys.length) {
        keys.forEach(function (key) {
          robot.keyDown(key);
        });
      }
      robot.mouseClick({left: true}, clickDelay);
      if (keys && keys.length) {
        keys.forEach(function (key) {
          robot.keyUp(key);
        });
      }
      return this.robotPromise;
    },

    doubleClick: function (/*Integer?*/ delay) {
      // summary:
      //    Requests a double left click action from the robot.
      var clickDelay = this.determineTiming(delay, this.defaultDelay);
      var doubleClickDelay = 100;
      robot.mouseClick({left: true}, clickDelay);
      robot.mouseClick({left: true}, doubleClickDelay);
      return this.robotPromise;
    },

    doubleClickElement: function (el, delay, duration) {
      // summary:
      //    Moves the mouse to the given element and double clicks it.
      this.mouseToElement(el, delay, duration);
      return this.doubleClick();
    },

    enterText: function(el, text, delay, duration) {
      // summary:
      //    Moves the mouse to the given element and enters the given text to the end of the input.
      //    The delay and duration of both the mouse move and the text input are the same, as given.
      return this._textAction(el, text, delay, duration, {appendAll: true});
    },

    replaceText: function(el, text, delay, duration) {
      // summary:
      //    Moves the mouse to the given input element and replaces whatever input text is present
      //    with the given text. The delay and duration of both the mouse move and the text input
      //    are the same, as given.
      return this._textAction(el, text, delay, duration, {replace: true});
    },

    _textAction: function (el, text, delay, duration, options) {
      // summary:
      //    Moves the mouse to the given input element and enters some text. The way in which the
      //    text is entered can be customised with the flags in the options object. The delay and
      //    duration are applied equally in whole to each of the click and input actions.
      // options:
      //    If replace is true, all existing text will be overwritten.
      //    If appendLine is true, the text will be appended to the current line (press End first).
      //    If appendAll is true, the text will be appended to the very end of the input (press
      //    Ctrl + End first).
      delay = this.determineTiming(delay, this.defaultDelay);
      duration = this.determineTiming(duration, this.defaultDuration);

      try {
        this.clickElement(el, delay, duration);
      } catch (error) {
        // Additional logging.
        options && console.error('_textAction failed with options:', options);
        throw new Error(error.message + ' (entering text: ' + text + ')');
      }
      if (options.replace) {
        robot.keyPress('a', delay, {ctrl: true});
      } else if (options.appendLine) {
        robot.keyPress(dojo.keys.END, delay);
      } else if (options.appendAll) {
        robot.keyPress(dojo.keys.END, delay, {ctrl: true});
      }

      robot.typeKeys(text, delay, duration);
      return this.robotPromise;
    },

    determineTiming: function (/*Integer?*/ timing, /*Integer*/ defaultTiming) {
      // summary:
      //    Returns the timing (delay or duration) an action should have given the optional
      //    specification. This cannot rely on simple truthiness, since 0 is a value timing value.
      return (timing || timing === 0) ? timing : defaultTiming;
    },

    determineElement: function (/*Element|Object*/ target) {
      // summary:
      //    Returns the HTML element that is, or is most intentionally contained within, the given
      //    target.
      // description:
      //    If target is an element, it will simply returned. However there are some cases where an
      //    action is legitimately requested on a non-element, such as a jQuery result object, or a
      //    unit-length array. In these cases, the element that was presumably intended is extracted
      //    and returned individually. If a list of multiple elements is given, only the first will
      //    be returned.
      if (!target) throw new Error('Unable to extract Element from empty object:', target);
      if (target instanceof Element) return target;
      if (target.length && target[0] instanceof Element) return target[0];
      throw new Error('Unable to extract Element from given object:', target);
    },

    _getElementCenter: function (/*Element*/ el) {
      // summary:
      //    Calculates the (x, y) coordinates of the center of the given element.
      var rect = el.getBoundingClientRect();
      return {x: rect.left + rect.width / 2, y: rect.top + rect.height / 2}; // Object
    },

    _evaluateQuery: function (/*String*/ q, /*Integer?*/ index) {
      // summary:
      //    Evaluates the given query and returns the element at the given index (default 0).
      index = index || 0;
      return query(q)[index];
    },

    _buildTest: function (name, test) {
      // summary:
      //    Creates a managed doh.Deferred to allow convenient implementation of asynchronous tests
      //    with non-trivial temporal requirements. The Deferred is stored in currentTestDeferred,
      //    which is used by RobotPromise to chain test deferrals, and the pass/fail methods to end
      //    a test manually.
      return lang.hitch(this, function () {
        this.currentTestName = name;
        this.currentTestDeferred = new doh.Deferred();
        lang.hitch(this, test)();
        return this.currentTestDeferred;
      });
    },

    pass: function () {
      // summary:
      //    Resolves the current asynchronous test's doh.Deferred indicating that the test is
      //    complete and successful.
      this._checkCurrentTest() && this.currentTestDeferred.callback();
    },

    fail: function (/*Object?*/ error) {
      // summary:
      //    Rejects the current asynchronous test's doh.Deferred indicating that the test has failed
      //    and need not continue.
      // description:
      //    Calling this.fail() is optional, since _buildTest already listens for assertion failures
      //    within the test runner.
      this._checkCurrentTest() && this.currentTestDeferred.errback(error);
    },

    _checkCurrentTest: function () {
      // summary:
      //    Checks the completion state of the current test as a prelude to attempting to complete
      //    the current test. If the test is not in a state to be completed (null or fulfilled),
      //    logs a warning and returns false. Otherwise returns true.
      var dfd = this.currentTestDeferred;
      if (!dfd) {
        console.warn('No test currently in progress to complete.');
        return false;
      }
      if (dfd.isResolved()) {
        console.warn('Can\'t complete current test ' + this.currentTestName + ': already passed.');
        return false;
      }
      if (dfd.isRejected()) {
        console.warn('Can\'t complete current test' + this.currentTestName + ': already failed.');
        return false;
      }
      return true;
    }

  });
});
