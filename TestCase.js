define([
  'dojo/_base/declare',
  'dojo/_base/lang'
], function (declare, lang) {

  return declare(null, {
    // name: String
    //    The name of this TestCase to display while testing.
    name: 'UNNAMED_TEST',
    // TODO(orlade): Generate more useful default name from module name.

    // fixedOrder: Boolean
    //    If true, tests are executed in the order they are defined, otherwise there is
    //    no guarantee of order.
    fixedOrder: true,

    constructor: function (/*Object|String*/ params) {
      // summary:
      //    Mixes in any parameters passed into the constructor.
      // params:
      //    Either a string specifying the name of the TestCase, or a map of
      //    properties to mix into the TestCase (declare handles this
      //    automatically).
      if (typeof params === 'string') {
        this.name = params;
      } else if (typeof params === 'object') {
        declare.safeMixin(this, params);
      }
    },

    setUpClass: function () {
      // summary:
      //    Executed before any tests have been run to acquire any
      //    computationally-expensive resources that may be shared between
      //    tests.
      //
      //    Note that DOH's setUp calls are synchronous, so any asynchronous actions executed will
      //    likely not be complete by the time the test starts.
    },

    setUp: function () {
      // summary:
      //    Executed before every test to provide a convenient encapsulation of
      //    common setup instructions.
      //
      //    Note that DOH's setUp calls are synchronous, so any asynchronous actions executed will
      //    likely not be complete by the time the test starts.
    },

    tearDown: function () {
      // summary:
      //    Executed after every test to provide a convenient encapsulation of
      //    common tear-down instructions.
    },

    tearDownClass: function () {
      // summary:
      //    Executed after every test has finished to clean up any resources
      //    acquired by beforeClass.
    },

    register: function (runner) {
      // summary:
      //    Registers any test methods of this class with the given test runner as sub-tests of the
      //    test group represented by this class.
      if (!runner) {
        throw new Error('No test runner provided for registration.');
      }
      var fixtures = this._buildFixtures();
      runner.register(this.name, fixtures, this.setUpClass, this.tearDownClass);
    },

    registerEach: function (runner, /*String?*/ first, /*String?*/ last) {
      // summary:
      //    Registers any test methods of this class with the given test runner as individual test
      //    groups. This is useful if you are forced to use TestCase within a doh.register() call,
      //    such as when loading HTML tests via require.toUrl().
      // description:
      //    Although DOH appears to support deep (> 2 levels of ) nesting of test groups, the GUI
      //    doesn't handle it well. Only the first- and second-level groups are displayed, and
      //    although any tests/subgroups of the second-level test cases are executed correctly, the
      //    results are not clearly reported in the GUI.
      //
      //    By registering these tests at the third-level and below as individual tests, they can be
      //    displayed one level higher while still following the useful TestCase construction
      //    method. This is most useful in the case where a test (or test module) file imports a
      //    browser test using doh.register(group, require.toUrl(path)). Using TestCase.register()
      //    would cause the test methods to be registered as group > TestCase.name > testMethod, and
      //    the results would aggregate all testMethods into the TestCase.name group. Using
      //    TestCase.registerEach() would display group > testMethod for each test method.
      //
      //    There is almost certainly a better way to do this, but it's better than nothing.
      // runner:
      //    The test runner with which to register the tests.
      // first:
      //    The first test method to run. This will execute both setUp and setUpClass before it is
      //    run itself. If not specified, first will default to the
      // last:
      //    The last test method to run. This will execute both tearDown and tearDownClass after it
      //    is run itself.
      if (!runner) {
        throw new Error('No test runner provided for registration.');
      }

      var fixtures = this._buildFixtures();
      if (!fixtures || fixtures.length == 0) return;

      var firstFixture = fixtures[0], lastFixture = fixtures[fixtures.length - 1];

      // Find (or choose) the first and last fixtures and remove them from the fixtures list.
//      first = first || fixtures[0].name;
//      last = last || fixtures[fixtures.length - 1].name;
//      if (fixtures.length == 1) {
//        firstFixture = lastFixture = fixtures[0];
//      } else if (first || last) {
//        firstFixture = ;
//        lastFixture = fixtures[fixtures.length - 1];
//        for (var i = 0, fixture; fixture = fixtures[i]; i++) {
////          console.error('!!! i', i);
//          if (fixture.name === first) {
//            firstFixture = fixture;
////            fixtures.slice(i--, 1);
//          } else if (fixture.name === last) {
//            lastFixture = fixture;
////            fixtures.slice(i--, 1);
//          }
//        }
//      }

      // Set up the first and last fixtures with the appropriate setUp and tearDown methods.
      var firstSetUp = firstFixture.setUp;
      var lastTearDown = lastFixture.tearDown;
      firstFixture.setUp = lang.hitch(this, function () {
        this.setUpClass();
        firstSetUp();
      });
      lastFixture.tearDown = lang.hitch(this, function () {
        lastTearDown();
        this.tearDownClass();
      });

      // Register the fixtures as test cases
      fixtures.forEach(lang.hitch(this, function (fixture) {
        runner.register(this.name, fixture);
      }));
    },

    testOrder: function (names) {
      // summary:
      //  Test names are passed and the returned array is the order they will execute.
      if (!this.fixedOrder) {
        // TODO(aramk): refactor shuffle into common lib for reuse

        // summary:
        //    Shuffles an array mutationally.
        // see:
        //    http://stackoverflow.com/questions/6274339
        var shuffle = function (o) {
          for (var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
        };

        shuffle(names);
      }
      return names;
    },

    _buildFixtures: function () {
      // summary:
      //    Converts each method of this TestCase starting with 'test' into a
      //    named DOH test fixture with setUp = before and tearDown = after.
      var isTestName = lang.hitch(this, function (name) {
        // summary:
        //    Returns whether the given name corresponds to a test method.
        return typeof this[name]/* === 'function'*/ && name.substr(0, 4) === 'test';
      });

      var testNames = this.testOrder(Object.keys(this).filter(isTestName));

      return testNames.map(lang.hitch(this, function (/*String*/ name) {
        var test = this._buildTest(name, this[name]);
        // summary:
        //    Converts test names into fixtures by creating a new object with a runTest property
        //    mapped to the named method, and delegating all other properties (such as setUp,
        //    tearDown and any custom utility methods) back to this TestCase.
        if (typeof test == 'function') {
          return lang.delegate(this, {
            name: name,
            runTest: test
          });
        } else if (typeof test == 'object') {
          return lang.delegate(this, lang.mixin({
            name: name
            // runTest must be defined
          }, test));
        } else {
          throw new Error('Test is not valid: ' + name);
        }
      }));
    },

    _buildTest: function (name, test) {
      // summary:
      //    Allows overriding the test function during building.
      return test;
    }
  });
});
