define([
], function () {
  // TODO(orlade): Declare class for mock handle.
  var Mocking = {
    mockMethod: function (obj, /*String*/ methodName, /*Function*/ replacement) {
      // summary:
      //    Replaces the method with the given name on the given object with the
      //    given replacement method.
      // obj:
      //    The object to mock.
      // methodName:
      //    The name of the method to replace.
      // replacement:
      //    The function to replace the target method with.
      var originalMethod = obj[methodName];
      var handle = {
        object: obj,
        methodName: methodName,
        originalMethod: originalMethod,
        reset: function () {
          Mocking.unmock(handle);
        }
      };
      obj[methodName] = replacement;
      return handle;
    },

    unmock: function (/*Object*/ handle) {
      // summary:
      //    Resets the method on the mocked object to whatever it was before
      //    mockMethod was called on it.
      // handle:
      //    The handle object created by mockMethod when it was previously
      //    called.
      handle.object[handle.methodName] = handle.originalMethod;
    }
  };

  return Mocking;
});
