# DOH Augmentation Module (DAM)

DAM is a custom extension of the DOH library (in Dojo's util package)
providing more convenient access to DOH's common methods.

## Usage

The DAM package (dam) should be checked out alongside util package, declared in
the Dojo config, and imported with require() or define() as usual.

## Licence

DAM is released under LGPLv3.
